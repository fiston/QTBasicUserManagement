CREATE TABLE IF NOT EXISTS users
(
    id                      UUID    NOT NULL,
    first_name              VARCHAR(255),
    last_name               VARCHAR(255),
    phone_number            VARCHAR(255),
    email                   VARCHAR(255),
    password                VARCHAR(255),
    username                VARCHAR(255),
    role                    VARCHAR(50),
    status                  VARCHAR(50),
    created_at              TIMESTAMP WITHOUT TIME ZONE,
    updated_at              TIMESTAMP WITHOUT TIME ZONE,
    account_non_expired     BOOLEAN NOT NULL,
    account_non_locked      BOOLEAN NOT NULL,
    credentials_non_expired BOOLEAN NOT NULL,
    enabled                 BOOLEAN NOT NULL,
    is_resetting_password   BOOLEAN NOT NULL,
    CONSTRAINT pk_users PRIMARY KEY (id)
);

INSERT INTO users (id,first_name,last_name,phone_number,email,"password",username,"role",status,created_at,updated_at,account_non_expired,account_non_locked,credentials_non_expired,enabled,is_resetting_password) VALUES
    ('cc47244c-0915-43c6-8653-48f6d7a4913c','admin','admin','+02507666666','dummy.email@provider.com','$2a$10$F.D38BPMHdNwiIJ6c6Z/jOeFJ7mZGiys8ISs7hcTKqGgvcynD7h8C','admin','ROLE_ADMIN','ACTIVE','2023-11-28 16:45:24.083','2023-11-28 16:45:24.083',true,true,true,true,false);


CREATE TABLE IF NOT EXISTS tasks
(
    id             UUID NOT NULL,
    name           VARCHAR(100),
    description    VARCHAR(255),
    status         VARCHAR(20),
    created_at     TIMESTAMP WITHOUT TIME ZONE,
    updated_at     TIMESTAMP WITHOUT TIME ZONE,
    assignees      VARCHAR(255),
    project        VARCHAR(255),
    priority       VARCHAR(10),
    fileAttachment VARCHAR(255),
    CONSTRAINT pk_tasks PRIMARY KEY (id)
);