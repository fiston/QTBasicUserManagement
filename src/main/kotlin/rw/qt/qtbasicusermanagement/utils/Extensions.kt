package rw.qt.qtbasicusermanagement.utils

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import rw.qt.qtbasicusermanagement.exceptions.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.NoSuchElementException

fun Throwable.handleAndThrowException() {

    when {
        localizedMessage.contains("InvalidFormatException") -> throw BadRequestException("INVALID_FORMAT" + this.message)
        localizedMessage.contains("ValueInstantiationException") -> throw BadRequestException("PARAMETER_MISSING")
        else -> throw this
    }
}

fun Throwable.toHttpStatus(): HttpStatus {
    return when (this) {

        is ParameterNeededException -> {
            HttpStatus.BAD_REQUEST
        }

        is HttpMessageNotReadableException -> {
            HttpStatus.BAD_REQUEST
        }

        is IllegalAccessException -> {
            HttpStatus.BAD_REQUEST
        }

        is IllegalArgumentException -> {
            HttpStatus.BAD_REQUEST
        }

        is DataIntegrityViolationException -> {
            HttpStatus.FORBIDDEN
        }

        is UnauthorizedException -> {
            HttpStatus.UNAUTHORIZED

        }

        is NotFoundException -> {
            HttpStatus.NOT_FOUND

        }

        is NoSuchElementException -> {
            HttpStatus.NOT_FOUND

        }

        is MethodArgumentTypeMismatchException -> {
            HttpStatus.NOT_FOUND

        }

        is EmptyResultDataAccessException -> {
            HttpStatus.NOT_FOUND

        }

        is InternalServerErrorException -> {
            HttpStatus.INTERNAL_SERVER_ERROR

        }

        is ForbiddenException -> {
            HttpStatus.FORBIDDEN

        }

        is InvalidJwtTokenException -> {
            HttpStatus.UNAUTHORIZED
        }

        is BadRequestException -> {
            HttpStatus.BAD_REQUEST
        }

        is DuplicateViolation -> {
            HttpStatus.CONFLICT
        }

        is AccountStatusException -> {
            HttpStatus.UNAUTHORIZED
        }

        is TokenRevokedException -> {
            HttpStatus.UNAUTHORIZED
        }

        else -> {
            HttpStatus.INTERNAL_SERVER_ERROR
        }
    }
}
fun String.toDate(): Date {
    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    formatter.timeZone = TimeZone.getTimeZone("UTC")
    return formatter.parse(this)
}