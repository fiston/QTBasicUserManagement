package rw.qt.qtbasicusermanagement.utils

enum class ERole{
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MODERATOR
}

enum class UserStatus{
    PENDING_APPROVAL,
    ACTIVE,
    REJECTED,
    DELETED
}

enum class Priorities {
    LOW,
    MEDIUM,
    HIGH
}