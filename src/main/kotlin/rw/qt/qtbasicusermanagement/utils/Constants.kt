package rw.qt.qtbasicusermanagement.utils

const val PHONE_REGEX = "^^\\+\\d{1,3}\\s?\\d{1,14}\$"