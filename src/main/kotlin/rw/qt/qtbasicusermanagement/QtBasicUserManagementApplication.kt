package rw.qt.qtbasicusermanagement

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QtBasicUserManagementApplication

fun main(args: Array<String>) {
    runApplication<QtBasicUserManagementApplication>(*args)
}
