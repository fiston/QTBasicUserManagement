package rw.qt.qtbasicusermanagement.configuration

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springdoc.core.models.GroupedOpenApi
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class SwaggerConfig {

    @Bean
    fun customOpenAPI(@Value("\${springdoc.version}") appVersion: String?): OpenAPI? {
        val apiKeyScheme = SecurityScheme()
            .type(SecurityScheme.Type.APIKEY)
            .name("Bearer")
            .`in`(SecurityScheme.In.HEADER)
        return OpenAPI()
            .info(
                Info().title("Abakoresha Service")
                    .version(appVersion)
                    .license(License().name("QT All rights reserved").url("#"))
                    .contact(Contact().url("https://qtglobal.rw"))
            )
            .components(Components().addSecuritySchemes("API_KEY_NAME", apiKeyScheme))
            .addSecurityItem(SecurityRequirement().addList("Bearer"))
    }


    @Bean
    fun loginOpenApi(): GroupedOpenApi {
        val paths = arrayOf("/**")

        return GroupedOpenApi.builder().group("QT_TEST").pathsToMatch(*paths)
            .build()
    }


}