package rw.qt.qtbasicusermanagement.configuration

import com.fasterxml.jackson.databind.*
import io.r2dbc.postgresql.PostgresqlConnectionConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.lang.NonNull
import rw.qt.qtbasicusermanagement.utils.DateToLocalDateTimeConverter
import rw.qt.qtbasicusermanagement.utils.LocalDateTimeToDateConverter


@Configuration
class ReactivePostgresConfig  : AbstractR2dbcConfiguration() {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Value("\${spring.r2dbc.username}")
    private lateinit var userName: String

    @Value("\${spring.r2dbc.password}")
    private lateinit var password: String

    @Value("\${spring.r2dbc.url}")
    private lateinit var url: String

    private val objectMapper: ObjectMapper=ObjectMapper()

    @Bean
    @NonNull
    override fun connectionFactory(): PostgresqlConnectionFactory {

        //r2dbc:postgresql://localhost:5432/loans
        val host = url.substring(url.indexOf("//") + 2, url.lastIndexOf(":"))
        val port = url.substring(url.lastIndexOf(":") + 1, url.lastIndexOf("/")).toInt()
        val database = url.substring(url.lastIndexOf("/") + 1)
        logger.debug("Setting data connectionFactory [host: {}, port: {}, database: {}]", host, port, database)
        return PostgresqlConnectionFactory(
            PostgresqlConnectionConfiguration.builder()
                .password(password)
                .username(userName)
                .database(database)
                .host(host)
                .port(port)
                .build()
        )
    }

    @Bean
    @NonNull
    override fun r2dbcCustomConversions(): R2dbcCustomConversions {
        val converters: MutableList<Converter<*, *>> = ArrayList()
        converters.add(LocalDateTimeToDateConverter())
        converters.add(DateToLocalDateTimeConverter())
        return R2dbcCustomConversions(storeConversions, converters)
    }

    @Bean
    fun jsonEncoder(): Jackson2JsonEncoder {
        return Jackson2JsonEncoder()
    }

    @Bean
    fun jsonDecoder(): Jackson2JsonDecoder {
        return Jackson2JsonDecoder()
    }


}