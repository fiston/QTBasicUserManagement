package rw.qt.qtbasicusermanagement.repository

import org.springframework.data.r2dbc.repository.R2dbcRepository
import rw.qt.qtbasicTasksmanagement.models.databases.Tasks
import java.util.*

interface TasksRepository: R2dbcRepository<Tasks, UUID> {
}