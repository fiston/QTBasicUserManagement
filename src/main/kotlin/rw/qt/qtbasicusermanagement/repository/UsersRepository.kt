package rw.qt.qtbasicusermanagement.repository

import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.models.databases.Users
import java.util.*

@Repository
interface UsersRepository :R2dbcRepository<Users, UUID>{

    fun findByUsername(username: String): Mono<Users>

    fun findByPhoneNumber(phoneNumber: String): Mono<Users>

    fun existsByEmail(email:String):Mono<Boolean>

    fun existsByPhoneNumber(phoneNumber: String):Mono<Boolean>
}