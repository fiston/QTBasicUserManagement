package rw.qt.qtbasicusermanagement.services.crud


import org.springframework.dao.DuplicateKeyException
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.exceptions.BadRequestException
import rw.qt.qtbasicusermanagement.exceptions.InternalServerErrorException
import rw.qt.qtbasicusermanagement.exceptions.NotFoundException

import java.util.*


interface IGenericService<T> {
    fun add(entity: T): Mono<T>
    fun delete(id: UUID): Mono<Void>
    fun getOne(id: UUID): Mono<T>
    fun exists(id: UUID): Mono<Boolean>
    fun findAll(): Flux<T>
}

@Service
abstract class GenericService<T : Any, K : R2dbcRepository<T, UUID>> : IGenericService<T> {

    abstract var repository: K

    override fun add(entity: T): Mono<T> {

        try {
            return repository.save(entity)
        } catch (ex: Exception) {
            if (ex is DuplicateKeyException)
                throw BadRequestException("PHONE_NUMBER_OR_EMAIL_ALREADY_USED")
            else
                throw InternalServerErrorException(ex.localizedMessage)
        }
    }

    override fun delete(id: UUID): Mono<Void> {
        return repository.deleteById(id)
    }

    override fun getOne(id: UUID): Mono<T> {
        val fallback: Mono<T> =
            Mono.error(NotFoundException("Object not found with  $id"))

        return repository.findById(id).switchIfEmpty(fallback)
    }

    override fun exists(id: UUID): Mono<Boolean> {
        return repository.existsById(id)
    }

    override fun findAll(): Flux<T> {
        return repository.findAll()
    }



}