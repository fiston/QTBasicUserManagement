package rw.qt.qtbasicusermanagement.services.crud

import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.models.databases.Users
import rw.qt.qtbasicusermanagement.repository.UsersRepository
import java.util.UUID


@Service
class UsersService (override var repository: UsersRepository): GenericService<Users, UsersRepository>() {

    fun findByUsername(username: String): Mono<Users> {
        return repository.findByUsername(username)
    }

    fun existsByEmail(email: String): Mono<Boolean> {
        return repository.existsByEmail(email)

    }

    fun existsByPhoneNumber(phoneNumber: String): Mono<Boolean> {
        return repository.existsByPhoneNumber(phoneNumber)

    }

    fun updateUsers(id:UUID, users: Users):Mono<Users>{
        return repository.findById(id)
            .flatMap { existingUsers ->
                val updatedUsers: Users = existingUsers
                    .copy(
                        firstName = users.firstName,
                        lastName = users.lastName,
                        phoneNumber = users.phoneNumber,
                        email = users.email,
                        password = users.password,
                        username = users.username,
                        role = users.role,
                        status = users.status,
                        accountNonExpired = users.accountNonExpired,
                        accountNonLocked = users.accountNonLocked,
                        credentialsNonExpired = users.credentialsNonExpired,
                        enabled = users.enabled
                    )
                repository.save(updatedUsers)
            }
    }
}