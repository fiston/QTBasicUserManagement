package rw.qt.qtbasicusermanagement.services

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.codec.multipart.FilePart
import org.springframework.http.codec.multipart.FormFieldPart
import org.springframework.http.codec.multipart.Part
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.qt.qtbasicTasksmanagement.models.databases.Tasks
import rw.qt.qtbasicusermanagement.repository.TasksRepository
import rw.qt.qtbasicusermanagement.utils.Priorities
import rw.qt.qtbasicusermanagement.utils.toDate
import java.io.File
import java.util.*

interface TasksServices {

    fun getAllTasks(): Flux<Tasks>

    fun getTaskById(id: UUID): Mono<Tasks>

    fun createTask(partMap: Map<String?, Part?>): Mono<Tasks>
}

@Service
class TasksServicesImpl(
    private var tasksRepository: TasksRepository
) : TasksServices {

    @Value("\${abakoresha_service.app.file.path}")
    lateinit var rootPath: String

    val logger: Logger = LoggerFactory.getLogger(TasksServicesImpl::class.java)


    override fun getAllTasks(): Flux<Tasks> {
        return tasksRepository.findAll()
    }

    override fun getTaskById(id: UUID): Mono<Tasks> {
        return tasksRepository.findById(id)
    }

    override fun createTask(partMap: Map<String?, Part?>): Mono<Tasks> {
        partMap.forEach { (partName: String?, value: Part?) ->
            logger.info(
                "Name: {}, value: {}",
                partName,
                value
            )
        }
        // Handle file
        val attachment = partMap["fileAttachment"] as FilePart?

        // Handle profile
        val profile = partMap["profile"] as FormFieldPart?


        val title = partMap["title"] as FormFieldPart?
        val description = partMap["description"] as FormFieldPart?
        val status = partMap["status"] as FormFieldPart?
        val createdAt = partMap["createdAt"] as FormFieldPart?
        val updatedAt = partMap["updatedAt"] as FormFieldPart?
        val assignees = partMap["assignees"] as FormFieldPart?
        val project = partMap["project"] as FormFieldPart?
        val priority = partMap["priority"] as FormFieldPart?
        val fileAttachment = partMap["fileAttachment"] as FormFieldPart?
        return tasksRepository.save(
            Tasks(
                title = title!!.value(),
                description = description!!.value(),
                status = status!!.value(),
                createdAt = createdAt!!.value().toDate(),
                updatedAt = updatedAt!!.value().toDate(),
                assignees = assignees!!.value(),
                project = project!!.value(),
                priority = Priorities.valueOf(priority!!.value()),
                fileAttachment = fileAttachment!!.value()

            )
        ).flatMap { saved ->
            logger.info("Saved: {}", saved)

            attachment?.transferTo(File("$rootPath/${saved.id}/${attachment.filename()}"))
                ?.then(Mono.fromCallable { saved })

        }

    }
}