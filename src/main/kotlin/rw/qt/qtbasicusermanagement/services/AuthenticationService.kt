package rw.qt.qtbasicusermanagement.services

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.LockedException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import rw.qt.qtbasicusermanagement.exceptions.*
import rw.qt.qtbasicusermanagement.models.databases.Users
import rw.qt.qtbasicusermanagement.models.requests.*
import rw.qt.qtbasicusermanagement.models.responses.AuthenticationResponse
import rw.qt.qtbasicusermanagement.models.responses.HttpDefaultResponse
import rw.qt.qtbasicusermanagement.models.responses.RegistrationResponse
import rw.qt.qtbasicusermanagement.security.JwtUtils
import rw.qt.qtbasicusermanagement.services.crud.UsersService
import rw.qt.qtbasicusermanagement.utils.UserStatus

import java.time.LocalTime
import java.util.*


interface AuthenticationService {

    fun login(
        authenticationRequest: AuthenticationRequest,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse>

    fun refreshToken(
        refreshToken: String,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse>

    fun logout(logoutRequest: LogoutRequest): Mono<HttpDefaultResponse>

    fun verifyPassword(
        verifyPasswordRequest: VerifyPasswordRequest,
        sessionData: MutableMap<String, String>
    ): Mono<HttpDefaultResponse>

    fun activateAccount(@Parameter(`in` = ParameterIn.QUERY, name = "userId")userId: UUID): Mono<HttpDefaultResponse>

    fun whoAmI(userToken: String): Mono<Users>

    fun listAllUsers(): Flux<Users>

    fun signup(profileRequest: UsersRequest): Mono<RegistrationResponse>

    fun voidUser(@Parameter(`in` = ParameterIn.PATH, name = "userId") userId: UUID): Mono<Users>

    fun confirmUser(@Parameter(`in` = ParameterIn.PATH, name = "id") userId: UUID): Mono<Users>

    fun updatePassword(
        @Parameter(`in` = ParameterIn.PATH, name = "id") userId: UUID,
        selfPasswordResetRequest: SelfPasswordResetRequest
    ): Mono<HttpDefaultResponse>

    fun changePassword(passwordResetRequest: PasswordResetRequest): Mono<HttpDefaultResponse>
}

@Service
class AuthenticationServiceImpl(
    private var usersService: UsersService,
    private var bCryptPasswordEncoder: BCryptPasswordEncoder,
    private var jwtUtils: JwtUtils,
) : AuthenticationService {

    private val logger: Logger = LoggerFactory.getLogger(AuthenticationService::class.java)

    override fun listAllUsers(): Flux<Users> {
        return usersService.findAll()
    }

    override fun signup(@RequestBody profileRequest: UsersRequest): Mono<RegistrationResponse> {
        val emailExistsMono = usersService.existsByEmail(profileRequest.email)
        val phoneExistsMono = usersService.existsByPhoneNumber(profileRequest.phoneNumber)

        return Mono.zip(emailExistsMono, phoneExistsMono)
            .flatMap { tuple ->
                val emailExists = tuple.t1
                val phoneExists = tuple.t2

                if (emailExists || phoneExists) {
                    // Either email or phone already exists, return an error response.
                    return@flatMap Mono.error<RegistrationResponse>(DuplicateViolation("USER_ALREADY_SAVED_CONFLICT"))
                } else {
                    // Neither email nor phone exists, proceed with user registration.
                    val user = usersService.add(
                        Users(
                            firstName = profileRequest.firstName,
                            lastName = profileRequest.lastName,
                            email = profileRequest.email,
                            phoneNumber = profileRequest.phoneNumber,
                            username = profileRequest.username.lowercase(Locale.getDefault()),
                            password = bCryptPasswordEncoder.encode(profileRequest.password),
                        ).setAsNew()
                    )
                    return@flatMap user.map { usr ->
                        RegistrationResponse("RECEIVED", usr.status, usr.id.toString())
                    }
                }
            }
    }

    override fun voidUser(userId: UUID): Mono<Users> {

        val user = usersService.getOne(userId)
        return user.flatMap {
            usersService.updateUsers(it.id!!, it.apply { status = UserStatus.DELETED })
        }.switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
    }

    override fun confirmUser(userId: UUID): Mono<Users> {
        val user = usersService.getOne(userId)
        return user.flatMap {
            usersService.updateUsers(userId, it.apply {
                status = UserStatus.ACTIVE
                enabled = true
            })
        }.switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
    }

    override fun updatePassword(userId: UUID, selfPasswordResetRequest: SelfPasswordResetRequest): Mono<HttpDefaultResponse> {
        return usersService.getOne(userId)
            .switchIfEmpty { Mono.error(NotFoundException("USER_NOT_FOUND")) }
            .flatMap { user ->
                checkPassword(user.password, selfPasswordResetRequest.oldPassword)
                usersService.updateUsers(
                    userId,
                    user.apply {
                        password = bCryptPasswordEncoder.encode(selfPasswordResetRequest.newPassword)
                        isResettingPassword = false
                    })
                    .flatMap {
                        Mono.just(
                            HttpDefaultResponse(
                                status = "200",
                                message = "PASSWORD_UPDATED",
                                LocalTime.now().toString()
                            )
                        )
                    }

            }
    }

    override fun changePassword(passwordResetRequest: PasswordResetRequest): Mono<HttpDefaultResponse> {
        return usersService.findByUsername(passwordResetRequest.username)
            .switchIfEmpty { Mono.error(NotFoundException("USER_NOT_FOUND")) }
            .flatMap { user ->
                usersService.updateUsers(
                    user.id!!,
                    user.apply {
                        password = bCryptPasswordEncoder.encode(passwordResetRequest.newPassword)
                        isResettingPassword = true
                    })
                    .flatMap {
                        Mono.just(
                            HttpDefaultResponse(
                                status = "200",
                                message = "PASSWORD_UPDATED",
                                LocalTime.now().toString()
                            )
                        )
                    }

            }
    }

    override fun login(
        authenticationRequest: AuthenticationRequest,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse> {
        val fallback: Mono<Users> =
            Mono.error(UnauthorizedException("No user account was found with username ${authenticationRequest.username.lowercase()}"))

        val user = usersService.findByUsername(authenticationRequest.username.lowercase()).switchIfEmpty(fallback)

        return user.map { usr ->
            logger.info("Login find user")
            checkPassword(usr.password, authenticationRequest.password)
            check(usr)
            val token = jwtUtils.generateSessionToken(usr, sessionData)

            logger.info("========++++++++++++= \n$token\n========++++++++++++= ")
            return@map AuthenticationResponse(
                token = token,
                refreshToken = token,
                id = usr.id.toString(),
                username = usr.username,
                email = usr.email,
                phoneNumber = usr.phoneNumber,
                roles = usr.role.name,
                isResettingPassword = usr.isResettingPassword

            )
        }.log()
            .doOnNext {
                if (it == null)
                    throw UnauthorizedException("BAD_CREDENTIAL")
            }
            .onErrorMap {
                logger.error("++++++++++++\n ${it}\n==========")

                throw UnauthorizedException("BAD_CREDENTIALS")
            }

    }

    override fun refreshToken(
        refreshToken: String,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse> {
        if (!jwtUtils.validateRefreshToken(refreshToken)) {
            throw UnauthorizedException("BAD_TOKEN")
        }

        val username = jwtUtils.getUserNameFromToken(refreshToken).lowercase()
        val userMono = usersService.findByUsername(username)
            .switchIfEmpty(Mono.error(UnauthorizedException("No user account was found with username $username")))

        return userMono.flatMap { user ->
            logger.info("User found: ${user.username}")

            check(user)

            val token = jwtUtils.generateSessionToken(user, sessionData)
            logger.info("New token generated: $token")

            Mono.just(
                AuthenticationResponse(
                    token = token,
                    refreshToken = token,
                    id = user.id.toString(),
                    username = user.username,
                    email = user.email,
                    phoneNumber = user.phoneNumber,
                    roles = user.role.name,
                    isResettingPassword = user.isResettingPassword
                )
            )
        }
            .doOnNext { authenticationResponse ->
                if (authenticationResponse == null) {
                    throw UnauthorizedException("BAD_CREDENTIAL")
                }
            }
            .onErrorMap { throwable ->
                logger.error("An error occurred during token refresh: ${throwable.message}")
                throwable.printStackTrace()
                UnauthorizedException("BAD_TOKEN")
            }
    }


    override fun logout(logoutRequest: LogoutRequest): Mono<HttpDefaultResponse> {
        val (accessToken, refreshToken) = logoutRequest
        val isSuccess = jwtUtils.logout(accessToken, refreshToken)

        val status = if (isSuccess) "200" else "400"
        val message = if (isSuccess) "LOGOUT_SUCCESSFUL" else "LOGOUT_FAILED"

        return Mono.just(HttpDefaultResponse(status, message, LocalTime.now().toString()))
    }

    override fun verifyPassword(
        verifyPasswordRequest: VerifyPasswordRequest,
        sessionData: MutableMap<String, String>
    ): Mono<HttpDefaultResponse> {
        val validateToken = jwtUtils.validatingToken(verifyPasswordRequest.accessToken, sessionData)
        val userMono = usersService.findByUsername(verifyPasswordRequest.username)
            .switchIfEmpty(Mono.error(UnauthorizedException("No user account was found with username ${verifyPasswordRequest.username}")))
        return userMono.map { usr ->
            logger.info("Login find user")
            checkPassword(usr.password, verifyPasswordRequest.password)
            HttpDefaultResponse(status = "200", message = "PASSOWRD_CORRECT", LocalTime.now().toString())
        }
    }

    override fun activateAccount(userId: UUID): Mono<HttpDefaultResponse> {
        return usersService.getOne(userId)
            .switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
            .flatMap { user ->
                usersService.updateUsers(userId, user.apply { enabled = true })
                    .map {
                        HttpDefaultResponse(
                            status = "200",
                            message = "ACCOUNT_ACTIVATED",
                            LocalTime.now().toString()
                        )
                    }
                    .onErrorResume { error ->
                        logger.error(error.localizedMessage)
                        Mono.just(
                            HttpDefaultResponse(
                                status = "400",
                                message = "ACCOUNT_ACTIVATION_FAILED",
                                LocalTime.now().toString()
                            )
                        )
                    }
            }
    }

    override fun whoAmI(userToken: String): Mono<Users> {
        val token = userToken.replace("Bearer ", "")
        if (jwtUtils.validateJwtToken(token)) {
            val username = jwtUtils.getUserNameFromToken(token)
            return usersService.findByUsername(username)
                .switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
        } else throw InvalidJwtTokenException("INVALID_TOKEN")
    }

    private fun check(user: Users) {

        if (!user.isAccountNonLocked) {

            throw LockedException("ACCOUNT_LOCKED")

        } else if (!user.isEnabled) {

            throw DisabledException("USER_DISABLED")

        } else if (!user.isAccountNonExpired) {

            throw AccountExpiredException("ACCOUNT_EXPIRED")

        } else if (!user.isCredentialsNonExpired) {

            throw CredentialsExpiredException("CREDENTIALS_EXPIRED")
        } else if (user.status == UserStatus.DELETED) {

            throw AccountStatusException("ACCOUNT_DELETED")
        } else if (user.status == UserStatus.PENDING_APPROVAL) {

            throw AccountStatusException("PENDING_APPROVAL")
        }
    }

    fun checkPassword(encryptedPassword: String, unencryptedPassword: String) {
        if (!bCryptPasswordEncoder.matches(unencryptedPassword, encryptedPassword))
            throw UnauthorizedException("BAD_CREDENTIALS")
    }


}

