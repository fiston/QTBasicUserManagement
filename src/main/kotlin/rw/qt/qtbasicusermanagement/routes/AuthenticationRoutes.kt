package rw.qt.qtbasicusermanagement.routes


import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RequestPredicates.*
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.function.server.ServerResponse
import rw.qt.qtbasicusermanagement.routes.handlers.AuthorizationHandler
import rw.qt.qtbasicusermanagement.routes.annotations.AuthenticationApiInfo


@Configuration(proxyBeanMethods = false)
class AuthenticationRoutes {

    @Bean
    @AuthenticationApiInfo
    fun authenticationRoute(authorizationHandler: AuthorizationHandler): RouterFunction<ServerResponse> {
        return RouterFunctions.route(
            POST("/login").and(accept(MediaType.APPLICATION_JSON)),
            authorizationHandler::login
        )
            .andRoute(
                GET("/who").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::whoAmI
            )
            .andRoute(
                GET("/refresh-token").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::refreshToken
            )
            .andRoute(
                GET("/logout").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::logout
            )
            .andRoute(
                POST("/verify-password").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::verifyPassword
            )
            .andRoute(
                GET("/admin/activate-account").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::activateAccount
            )
            .andRoute(
                POST("/signup").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::signup
            )
            .andRoute(
                DELETE("/admin/void-user/{userId}").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::voidUser
            )
            .andRoute(
                GET("/admin/users/{id}/confirm").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::confirmUser
            )
            .andRoute(
                GET("/admin/users").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::listAllUsers
            )
            .andRoute(PATCH("/users/{id}").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::selfUpdatePassword
            )
            .andRoute(PATCH("/admin/users").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::changePassword
            )
    }

}