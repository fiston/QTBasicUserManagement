package rw.qt.qtbasicusermanagement.routes.annotations

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.media.Schema
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import rw.qt.qtbasicusermanagement.models.requests.TasksRequests
import rw.qt.qtbasicusermanagement.routes.handlers.TasksHandler
import rw.qt.qtbasicusermanagement.services.TasksServices


@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@RouterOperations(
    RouterOperation(
        path = "/twese/create-task",
        beanClass = TasksServices::class,
        beanMethod = "createTask",
        method = arrayOf(RequestMethod.POST),
        operation = Operation(
            operationId = "createFile",
            summary = "Create a new task",
            description = "Create a new task",
            tags = arrayOf("TASKS"),
            requestBody = RequestBody(
                required = true,
                content = arrayOf(
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = TasksRequests::class)
                    )
                )
            ),
        )
    ),
    RouterOperation(
        path = "/twese/tasks",
        beanClass = TasksServices::class,
        beanMethod = "getAllTasks",
        method = arrayOf(RequestMethod.GET),
        operation = Operation(
            operationId = "getAllTasks",
            summary = "Get all tasks",
            description = "Get all tasks",
            tags = arrayOf("TASKS"),
        )
    ),
    RouterOperation(
        path = "/twese/tasks/{id}",
        beanClass = TasksServices::class,
        beanMethod = "getTaskById",
        method = arrayOf(RequestMethod.GET),
        operation = Operation(
            operationId = "getTaskById",
            parameters = [
                Parameter(name = "id", description = "Task id", required = true, example = "1", `in` = ParameterIn.PATH)
                         ],
            summary = "Get task by id",
            description = "Get task by id",
            tags = arrayOf("TASKS"),
        )
    )
)
annotation class TasksApiInfo