package rw.qt.qtbasicusermanagement.routes.annotations

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMethod
import rw.qt.qtbasicusermanagement.exceptions.GlobalExceptionHandler
import rw.qt.qtbasicusermanagement.models.databases.Users
import rw.qt.qtbasicusermanagement.models.requests.*
import rw.qt.qtbasicusermanagement.models.responses.AuthenticationResponse
import rw.qt.qtbasicusermanagement.models.responses.HttpDefaultResponse
import rw.qt.qtbasicusermanagement.models.responses.UsersResponse


@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@RouterOperations(
    RouterOperation(
        method = arrayOf(RequestMethod.POST),
        path = "/login",
        operation = Operation(
            description = "logging in",
            requestBody = RequestBody(
                description = "logging in",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = AuthenticationRequest::class),
                        examples = [ExampleObject()]
                    )
                ]

            ),
            operationId = "login",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Authentication Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = AuthenticationResponse::class)
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.GET),
        path = "/who",
        operation = Operation(
            description = "who am i",
            operationId = "whoami",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Users Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = UsersResponse::class), examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.GET),
        path = "/admin/users",
        operation = Operation(
            description = "Get all Users",
            operationId = "listAllUsers",
            tags = arrayOf("ADMINISTRATOR"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Users Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = UsersResponse::class)),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.POST),
        path = "/signup",
        operation = Operation(
            description = "User Signup",
            requestBody = RequestBody(
                description = "logging in",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = UsersRequest::class),
                        examples = [ExampleObject()]
                    )

                ]

            ),
            operationId = "signup",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Signup Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = AuthenticationResponse::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.GET),
        path = "/admin/users/{id}/confirm",
        operation = Operation(
            description = "Confirm Users",
            parameters = [Parameter(
                name = "id",
                description = "UserID to confirm",
                `in` = ParameterIn.PATH,
                required = true
            )],
            operationId = "confirmUser",
            tags = arrayOf("ADMINISTRATOR"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Confirm Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = Users::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.DELETE),
        path = "/admin/void-user/{userId}",
        operation = Operation(
            description = "voidUser",
            parameters = [Parameter(
                name = "userId",
                description = "UserID to DELETE",
                `in` = ParameterIn.PATH,
                required = true
            )],
            operationId = "voidUser",
            tags = arrayOf("ADMINISTRATOR"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Voided User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = Users::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.PATCH),
        path = "/admin/activate-account",
        operation = Operation(
            description = "activateAccount",
            parameters = [Parameter(
                name = "userId",
                description = "userId to activate",
                `in` = ParameterIn.QUERY,
                required = true
            )],
            operationId = "activateAccount",
            tags = arrayOf("ADMINISTRATOR"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Activated User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = HttpDefaultResponse::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.POST),
        path = "/verify-password",
        operation = Operation(
            description = "verifyPassword",
            requestBody = RequestBody(
                description = "Verify Password",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = VerifyPasswordRequest::class),
                        examples = [ExampleObject()]
                    )

                ]

            ),
            operationId = "verifyPassword",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Verify Password Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = Users::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.PATCH),
        path = "/users/{id}",
        operation = Operation(
            description = "selfUpdatePassword",
            parameters = [Parameter(
                name = "id",
                description = "UserID to update",
                `in` = ParameterIn.PATH,
                required = true
            )],
            requestBody = RequestBody(
                description = "logging in",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = SelfPasswordResetRequest::class),
                        examples = [ExampleObject()]
                    )

                ]

            ),
            operationId = "selfUpdatePassword",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Updated User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = HttpDefaultResponse::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.PATCH),
        path = "/admin/users",
        operation = Operation(
            description = "changePassword",
            requestBody = RequestBody(
                description = "logging in",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = PasswordResetRequest::class),
                        examples = [ExampleObject()]
                    )

                ]
            ),
            operationId = "changePassword",
            tags = arrayOf("ADMINISTRATOR"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Updated User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = HttpDefaultResponse::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = GlobalExceptionHandler.HttpError::class)),
                    examples = [ExampleObject()]
                )]
            )]
        )
    )
)
annotation class AuthenticationApiInfo 