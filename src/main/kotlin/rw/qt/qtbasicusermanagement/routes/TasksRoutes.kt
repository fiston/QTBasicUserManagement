package rw.qt.qtbasicusermanagement.routes

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RequestPredicates
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.function.server.ServerResponse
import rw.qt.qtbasicusermanagement.routes.annotations.TasksApiInfo
import rw.qt.qtbasicusermanagement.routes.handlers.TasksHandler


@Configuration(proxyBeanMethods = false)
class TasksRoutes {

    @Bean
    @TasksApiInfo
    fun taskRoute(authorizationHandler: TasksHandler): RouterFunction<ServerResponse> {
        return RouterFunctions.route(
            RequestPredicates.POST("/twese/create-task").and(RequestPredicates.accept(MediaType.MULTIPART_FORM_DATA)),
            authorizationHandler::createTask
        ).andRoute(
            RequestPredicates.GET("/twese/tasks").and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
            authorizationHandler::getAllTasks
        ).andRoute(
            RequestPredicates.GET("/twese/tasks/{id}").and(RequestPredicates.accept(MediaType.APPLICATION_JSON)),
            authorizationHandler::getTaskById
        )
    }
}