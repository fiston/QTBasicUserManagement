package rw.qt.qtbasicusermanagement.routes.handlers

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.exceptions.UnauthorizedException
import rw.qt.qtbasicusermanagement.utils.BodyValidator
import rw.qt.qtbasicusermanagement.utils.handleAndThrowException

import java.util.*

abstract class BasicHandlers {

    @Autowired
    lateinit var bodyValidator: BodyValidator

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    inline fun <reified T, reified R> processPost(
        request: ServerRequest,
        crossinline processor: (T) -> Mono<R>,
        crossinline errorLogger: (Throwable) -> Unit

    ): Mono<ServerResponse> {
        return bodyValidator.withValidBody(
            { body ->
                body.flatMap {
                    handleResponse<Any>(
                        processor(it)
                    )

                }
            }, request, T::class.java
        ).doOnError {
            errorLogger(it)
            it.handleAndThrowException()
        }
    }

    fun <T> handleRequestWithValidation(
        request: ServerRequest,
        requestType: Class<T>,
        processingFunction: (T) -> Mono<ServerResponse>
    ): Mono<ServerResponse> {
        return bodyValidator.withValidBody(
            { body ->
                body.flatMap { requestBody ->
                    processingFunction.invoke(requestBody)
                }
            },
            request,
            requestType
        ).onErrorMap {
            it.printStackTrace()
            throw UnauthorizedException(it.localizedMessage)
        }
    }

    inline fun <reified T> processGetRequest(
        request: ServerRequest,
        producer: (UUID?) -> Flux<T>,
        crossinline errorLogger: (Throwable) -> Unit
    ): Mono<ServerResponse> {
        val id = request.queryParam("id")
            .map { UUID.fromString(it) }
            .orElse(null)

        return ServerResponse.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromProducer(producer(id), T::class.java))
            .doOnError {
                errorLogger(it)
                it.handleAndThrowException()
            }
    }

    inline fun <reified T, reified R> processPostWithSessionData(
        request: ServerRequest,
        crossinline processor: (T, MutableMap<String, String>) -> Mono<R>,
        sessionData: MutableMap<String, String>,
        crossinline errorLogger: (Throwable) -> Unit
    ): Mono<ServerResponse> {
        return bodyValidator.withValidBody(
            { body ->
                body.flatMap {
                    handleResponse<R>(processor(it, sessionData))
                }
            }, request, T::class.java
        ).doOnError {
            errorLogger(it)
            it.handleAndThrowException()
        }
    }

    inline fun <reified T> handleResponse(responseMono: Any): Mono<ServerResponse> =
        when (responseMono) {
            is Mono<*> -> {
                processMono<T>(responseMono)
            }

            is Flux<*> -> {
                processFlux<T>(responseMono)
            }

            else -> {
                throw IllegalArgumentException("Unsupported response type: ${responseMono.javaClass}")
            }
        }


    inline fun <reified T> processMono(mono: Mono<*>): Mono<ServerResponse> {
        return mono
            .flatMap { responseData ->
                ServerResponse
                    .ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromProducer(Mono.just(responseData), T::class.java))
            }
            .onErrorResume { error ->
                handleErrorResponse(error)
            }
    }

    inline fun <reified T> processFlux(flux: Flux<*>): Mono<ServerResponse> {
        return ServerResponse
            .ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                BodyInserters.fromProducer(
                    flux,
                    T::class.java
                )
            )
            .onErrorResume { error ->
                handleErrorResponse(error)
            }
    }

    fun handleErrorResponse(error: Throwable): Mono<ServerResponse> {
        logger.error("Error occurred: ${error::class.java}")
        throw error

    }
}