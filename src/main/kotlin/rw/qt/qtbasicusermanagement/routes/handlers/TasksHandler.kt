package rw.qt.qtbasicusermanagement.routes.handlers

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.codec.multipart.Part
import org.springframework.stereotype.Component
import org.springframework.util.MultiValueMap
import org.springframework.web.reactive.function.BodyExtractors
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.qt.qtbasicTasksmanagement.models.databases.Tasks
import rw.qt.qtbasicusermanagement.services.TasksServices
import java.util.*


interface TasksHandler {

    fun createTask(request: ServerRequest): Mono<ServerResponse>

    fun getAllTasks(request: ServerRequest): Mono<ServerResponse>

    fun getTaskById(request: ServerRequest): Mono<ServerResponse>


}

@Component
class TasksHandlerImpl(
    private var tasksServices: TasksServices
) : TasksHandler, BasicHandlers() {


   override fun createTask(request: ServerRequest): Mono<ServerResponse> {
        return request.body<Mono<MultiValueMap<String?, Part?>>>(
            BodyExtractors.toMultipartData()
        ).flatMap<Any> { parts: MultiValueMap<String?, Part?> ->
            val partMap: Map<String?, Part?> = parts.toSingleValueMap()

            handleResponse<Tasks>(tasksServices.createTask(partMap))
        }
            .then(ServerResponse.ok().bodyValue("Task Saved"))
    }

    override fun getAllTasks(request: ServerRequest): Mono<ServerResponse> {
        return handleResponse<Tasks>(tasksServices.getAllTasks())
    }

    override fun getTaskById(request: ServerRequest): Mono<ServerResponse> {
        val id=request.pathVariable("id")
        return handleResponse<Tasks>(tasksServices.getTaskById(UUID.fromString(id)))
    }
}