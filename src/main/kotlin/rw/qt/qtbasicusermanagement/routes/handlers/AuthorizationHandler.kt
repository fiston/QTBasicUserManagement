package rw.qt.qtbasicusermanagement.routes.handlers

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.exceptions.BadRequestException
import rw.qt.qtbasicusermanagement.models.databases.Users
import rw.qt.qtbasicusermanagement.models.requests.*
import rw.qt.qtbasicusermanagement.models.responses.AuthenticationResponse
import rw.qt.qtbasicusermanagement.models.responses.HttpDefaultResponse
import rw.qt.qtbasicusermanagement.models.responses.RegistrationResponse
import rw.qt.qtbasicusermanagement.services.AuthenticationService

import java.util.*

interface AuthorizationHandler {

    fun refreshToken(request: ServerRequest): Mono<ServerResponse>

    fun whoAmI(request: ServerRequest): Mono<ServerResponse>

    fun login(request: ServerRequest): Mono<ServerResponse>

    fun logout(request: ServerRequest): Mono<ServerResponse>

    fun signup(request: ServerRequest): Mono<ServerResponse>

    fun activateAccount(request: ServerRequest): Mono<ServerResponse>

    fun verifyPassword(request: ServerRequest): Mono<ServerResponse>

    fun listAllUsers(request: ServerRequest): Mono<ServerResponse>

    fun voidUser(request: ServerRequest): Mono<ServerResponse>

    fun confirmUser(request: ServerRequest): Mono<ServerResponse>

    fun selfUpdatePassword(request: ServerRequest): Mono<ServerResponse>

    fun changePassword(request: ServerRequest): Mono<ServerResponse>
}

@Component
class AuthorizationHandlerImpl(
    private var authenticationService: AuthenticationService
) : AuthorizationHandler, BasicHandlers() {
     val log: Logger = LoggerFactory.getLogger(this::class.java)

    override fun refreshToken(request: ServerRequest): Mono<ServerResponse> {
        val token = extractAuthToken(request)
        val sessionData: MutableMap<String, String> = generateSessionData(request)
        val errorLogger: (Throwable) -> Unit = { logger.error(it.message) }

        return processPostWithSessionData<String, AuthenticationResponse>(
            request,
            { _, sData ->
                authenticationService.refreshToken(token, sData)
            },
            sessionData,
            errorLogger
        )
    }

    override fun whoAmI(request: ServerRequest): Mono<ServerResponse> {
        val token = extractAuthToken(request)
        return handleResponse<Users>(authenticationService.whoAmI(token))
    }


    override fun login(request: ServerRequest): Mono<ServerResponse> {
        logger.info("======== Login ========")

        val sessionData: MutableMap<String, String> = generateSessionData(request)
        val errorLogger: (Throwable) -> Unit = { logger.error(it.message) }

        return processPostWithSessionData<AuthenticationRequest, AuthenticationResponse>(
            request,
            { loginRequest, sData ->
                authenticationService.login(loginRequest, sData)
            },
            sessionData,
            errorLogger
        )
    }


    override fun logout(request: ServerRequest): Mono<ServerResponse> {
        val sessionData: MutableMap<String, String> = generateSessionData(request)
        val errorLogger: (Throwable) -> Unit = { logger.error(it.message) }

        return processPostWithSessionData<LogoutRequest, HttpDefaultResponse>(
            request,
            { logoutRequest, _ ->
                authenticationService.logout(logoutRequest)
            },
            sessionData,
            errorLogger
        )
    }

    override fun signup(request: ServerRequest): Mono<ServerResponse> {
        val sessionData: MutableMap<String, String> = generateSessionData(request)
        val errorLogger: (Throwable) -> Unit = { logger.error(it.message) }

        return processPostWithSessionData<UsersRequest, RegistrationResponse>(
            request,
            { logoutRequest, _ ->
                authenticationService.signup(logoutRequest)
            },
            sessionData,
            errorLogger
        )
    }

    override fun activateAccount(request: ServerRequest): Mono<ServerResponse> {
        val userId = extractUserId(request) ?: return handleErrorResponse(BadRequestException("USER_ID_CANNOT_BE_NULL"))
        return handleResponse<HttpDefaultResponse>(authenticationService.activateAccount(userId))
    }

    override fun verifyPassword(request: ServerRequest): Mono<ServerResponse> {
        val sessionData: MutableMap<String, String> = generateSessionData(request)
        val errorLogger: (Throwable) -> Unit = { logger.error(it.message) }

        return processPostWithSessionData<VerifyPasswordRequest, HttpDefaultResponse>(
            request,
            { logoutRequest, sData ->
                authenticationService.verifyPassword(logoutRequest, sData)
            },
            sessionData,
            errorLogger
        )
    }

    override fun listAllUsers(request: ServerRequest): Mono<ServerResponse> {
        return handleResponse<Users>(authenticationService.listAllUsers())
    }

    override fun voidUser(request: ServerRequest): Mono<ServerResponse> {
        val userId = request.pathVariable("userId")

        return handleResponse<Users>(authenticationService.voidUser(UUID.fromString(userId)))
    }

    override fun confirmUser(request: ServerRequest): Mono<ServerResponse> {
        val userId = request.pathVariable("id")
        return handleResponse<Users>(authenticationService.confirmUser(UUID.fromString(userId)))
    }

    override fun selfUpdatePassword(request: ServerRequest): Mono<ServerResponse> {
        val userId = request.pathVariable("id")
        val sessionData: MutableMap<String, String> = generateSessionData(request)
        val errorLogger: (Throwable) -> Unit = { logger.error(it.message) }

        return processPostWithSessionData<SelfPasswordResetRequest, HttpDefaultResponse>(
            request,
            { verifyPasswordRequest, _ ->
                authenticationService.updatePassword(
                    UUID.fromString(userId),
                    verifyPasswordRequest
                )
            },
            sessionData,
            errorLogger
        )
    }

    override fun changePassword(request: ServerRequest): Mono<ServerResponse> {
        val userId = request.pathVariable("id")
        val sessionData: MutableMap<String, String> = generateSessionData(request)
        val errorLogger: (Throwable) -> Unit = { logger.error(it.message) }

        return processPostWithSessionData<PasswordResetRequest, HttpDefaultResponse>(
            request,
            { verifyPasswordRequest, _ ->
                authenticationService.changePassword(
                    verifyPasswordRequest
                )
            },
            sessionData,
            errorLogger
        )
    }

    private fun extractAuthToken(request: ServerRequest): String {
        return request.headers().header("Authorization").firstOrNull() ?: ""
    }

    private fun extractUserId(request: ServerRequest): UUID? {
        return request.queryParam("userId").map { UUID.fromString(it) }.orElse(null)
    }


    private fun generateSessionData(request: ServerRequest): MutableMap<String, String> {
        val sessionData: MutableMap<String, String> = HashMap()
        sessionData["ipAddress"] = request.remoteAddress().get().address.hostAddress
        sessionData["User-Agent"] = request.headers().firstHeader("User-Agent").orEmpty()
        return sessionData
    }
}