package rw.qt.qtbasicusermanagement.security

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authorization.AuthorizationDecision
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authorization.AuthorizationContext
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsConfigurationSource
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.security.AuthenticationManager
import rw.qt.qtbasicusermanagement.security.CorsFilter
import rw.qt.qtbasicusermanagement.security.SecurityContextRepository
import rw.qt.qtbasicusermanagement.services.crud.UsersService


@Configuration
@EnableWebFluxSecurity
//@EnableReactiveMethodSecurity
class WebSecurityConfiguration {

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    @Autowired
    private lateinit var securityContextRepository: SecurityContextRepository

    @Autowired
    private lateinit var userRepository: UsersService

    private val logger: Logger = LoggerFactory.getLogger(WebSecurityConfiguration::class.java)


    @Bean
    fun springSecurityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain? {

        http // ...
            .securityContextRepository(securityContextRepository)
            .authenticationManager(authenticationManager)
            .authorizeExchange { exchanges: AuthorizeExchangeSpec ->
                exchanges // any URL that starts with /admin/ requires the role "ROLE_ADMIN"
                    .pathMatchers("/admin/**").hasRole("ADMIN")
                    .pathMatchers("/twese/**").hasAnyRole("ADMIN", "USER")
                    // a POST to /users requires the role "USER_POST"
                    .pathMatchers(HttpMethod.POST, "/users")
                    .hasAuthority("ADMIN")
                    .pathMatchers("/users/who").hasAuthority("USER")
                    .pathMatchers("/users/{username}")   // a request to /users/{username} requires the current authentication's username to be equal to the {username}
                    .access { authentication: Mono<Authentication>, context: AuthorizationContext ->
                        authentication
                            .map(Authentication::getName)
                            .map{ username: String ->
                                username == context.variables["username"]
                            }
                            .map{ granted: Boolean ->
                                AuthorizationDecision(
                                    granted
                                )
                            }
                    } // allows providing a custom matching strategy that requires the role "ROLE_CUSTOM"
                    .pathMatchers("/",
                        "/signup",
                        "/login",
                        "/v3/api-docs/**",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/messaging/v3/api-docs",
                        "/all-users",
                        "/actuator/**",
                        "/actuator/info",
                        "/actuator/health")
                    .permitAll()
                    .anyExchange().authenticated()
            }.csrf().disable()
        return http.build()
    }

    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun userDetailsService(): ReactiveUserDetailsService? {
        return ReactiveUserDetailsService { username: String -> userRepository.findByUsername(username).map { it as UserDetails } }
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.addAllowedOrigin("*")
        configuration.addAllowedHeader("*")
        configuration.addAllowedMethod("*")
        configuration.maxAge = 3600L
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

    @Bean
    fun corsFilters(): CorsFilter {
        return CorsFilter(corsConfigurationSource())
    }
}