package rw.qt.qtbasicusermanagement.security

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.exceptions.BadRequestException
import rw.qt.qtbasicusermanagement.exceptions.UnauthorizedException


@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
class TokenValidatorFilter(private val jwtUtil: JwtUtils) : WebFilter {
    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val request = exchange.request
        val headers: HttpHeaders = request.headers

        val ipAddress = request.remoteAddress?.address?.hostAddress.orEmpty()
        val userAgent = headers.getFirst("User-Agent").orEmpty()
        val authToken: String = headers.getFirst("Authorization").orEmpty()


        return if (ipAddress.isEmpty() || userAgent.isEmpty()) {
            Mono.error(BadRequestException("SOMETHING IS FISHY"))
        } else {
            val sessionData: MutableMap<String, String> = HashMap()
            sessionData["ipAddress"] = ipAddress
            sessionData["User-Agent"] = userAgent
            if (authToken.isNotEmpty()) {
                return if (jwtUtil.validatingToken(authToken.replace("Bearer ",""), sessionData))
                    chain.filter(exchange)
                else
                    Mono.error(UnauthorizedException("DIFFERENT_COMPUTER"))
            } else
                chain.filter(exchange)
        }


    }
}
