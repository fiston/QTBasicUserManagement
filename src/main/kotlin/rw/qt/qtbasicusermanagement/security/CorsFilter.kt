package rw.qt.qtbasicusermanagement.security

import org.springframework.stereotype.Component
import org.springframework.web.cors.reactive.CorsConfigurationSource
import org.springframework.web.cors.reactive.CorsWebFilter

@Component
class CorsFilter(configSource: CorsConfigurationSource) : CorsWebFilter(configSource)