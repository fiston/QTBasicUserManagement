package rw.qt.qtbasicusermanagement.security

import io.jsonwebtoken.Claims
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import rw.qt.qtbasicusermanagement.utils.ERole
import rw.qt.qtbasicusermanagement.security.JwtUtils
import java.util.stream.Collectors


@Component
class AuthenticationManager(private val jwtUtil: JwtUtils) : ReactiveAuthenticationManager {

    override fun authenticate(authentication: Authentication): Mono<Authentication> {
        val authToken: String = authentication.credentials.toString()
        val username: String? = try {
            jwtUtil.getUserNameFromToken(authToken)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
        return if (username != null && jwtUtil.validateToken(authToken)) {
            val claims: Claims = jwtUtil.getAllClaimsFromToken(authToken)
            val rolesMap: List<String> = listOf<String>(
                claims.get(
                    "role",
                    String::class.java
                )
            )

            val roles: MutableList<ERole> = ArrayList()
            for (roleMap in rolesMap) {
                roles.add(ERole.valueOf(roleMap))
            }
            val auth = UsernamePasswordAuthenticationToken(
                username,
                null,
                roles.stream().map { authority: ERole ->
                    SimpleGrantedAuthority(
                        authority.name
                    )
                }.collect(Collectors.toList())
            )
            Mono.just(auth)
        } else {
            println("Error ${ jwtUtil.validateToken(authToken)} ====== $username")
            Mono.empty()
        }
    }



}

