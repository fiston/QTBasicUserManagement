package rw.qt.qtbasicusermanagement.security

import io.jsonwebtoken.*
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import jakarta.xml.bind.DatatypeConverter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import rw.qt.qtbasicusermanagement.exceptions.TokenRevokedException
import rw.qt.qtbasicusermanagement.models.databases.Users
import java.security.Key
import java.security.SignatureException
import java.util.*
import java.util.concurrent.TimeUnit


@Component
class JwtUtils {
    @Value("\${abakoresha_service.app.jwtSecret}")
    private val jwtSecret: String? = null

    private val logger: Logger = LoggerFactory.getLogger(JwtUtils::class.java)

    @Autowired
    private lateinit var redisTemplate: RedisTemplate<String, Any>

    @Value("\${abakoresha_service.app.jwtExpirationMs}")
    private val jwtExpirationMs = 0

    private val refreshTokenName: String = "REFRESH"

    fun getAllClaimsFromToken(token: String): Claims {
        return Jwts.parserBuilder()
            .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
            .build()
            .parseClaimsJws(token).body
    }

    fun generateJwtToken(authentication: Authentication): String {

        val userPrincipal = authentication.principal as Users
        return Jwts.builder().setSubject(userPrincipal.username)
            .setIssuedAt(Date())
            .setExpiration(Date(Date().time + jwtExpirationMs))
            .signWith(
                getSignInKey(), SignatureAlgorithm.HS256
            ).compact()
    }

    fun getUserNameFromToken(token: String): String {
        return Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(token).body.subject
    }

    fun validateJwtToken(authToken: String): Boolean {
        if (!validateToken(authToken))
            return false
        try {
            Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(authToken)
            return true
        } catch (e: SignatureException) {
            logger.error("Invalid JWT Signature: {}", e.message)
        } catch (e: MalformedJwtException) {
            logger.error("Invalid JWT token: {}", e.message)
        } catch (e: ExpiredJwtException) {
            logger.error("JWT token is expired: {}", e.message)
        } catch (e: UnsupportedJwtException) {
            logger.error("JWT token is unsupported: {}", e.message)
        } catch (e: IllegalArgumentException) {
            logger.error("JWT claims string is empty: {}", e.message)
        }
        return false
    }

    fun getExpirationDateFromToken(token: String): Date {
        return getAllClaimsFromToken(token).expiration
    }

    private fun isTokenExpired(token: String): Boolean {
        val expiration = getExpirationDateFromToken(token)
        return expiration.before(Date())
    }

    fun generateToken(user: Users): String {
        val claims: MutableMap<String, Any> = HashMap()
        claims["role"] = user.role
        return doGenerateToken(claims, user.username)
    }

    fun generateRefreshToken(user: Users): String {

        return doGenerateRefreshToken(user.username)
    }

    fun validateRefreshToken(token: String): Boolean {
        val claims = getAllClaimsFromToken(token)
        val tokenType = claims["type"] as? String

        return tokenType == refreshTokenName && isTokenExpired(claims) && isTokenValid(token)
    }

    private fun isTokenExpired(claims: Claims): Boolean {
        val expiration = claims.expiration
        return expiration.before(Date())
    }

    private fun doGenerateToken(claims: Map<String, Any>, username: String): String {
        val expirationTimeLong: Long = jwtExpirationMs.toLong() //in second
        val createdDate = Date()
        val expirationDate = Date(createdDate.time + expirationTimeLong * 1000)
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(username)
            .setIssuedAt(createdDate)
            .setExpiration(expirationDate)
            .signWith(
                getSignInKey(), SignatureAlgorithm.HS256
            ).compact()
    }

    private fun getSignInKey(): Key {
        val keyBytes = Decoders.BASE64.decode(jwtSecret)
        return Keys.hmacShaKeyFor(keyBytes)
    }

    private fun doGenerateRefreshToken(username: String): String {
        val expirationTimeLong: Long = jwtExpirationMs.toLong() //in second
        val createdDate = Date()
        val expirationDate = Date(createdDate.time + expirationTimeLong * 1000)
        val claims: MutableMap<String, Any> = HashMap()
        claims["type"] = refreshTokenName
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(username)
            .setIssuedAt(createdDate)
            .setExpiration(expirationDate)
            .signWith(
                getSignInKey(), SignatureAlgorithm.HS256
            ).compact()
    }

    fun validateToken(token: String): Boolean {
        logger.info("is token valid ${isTokenValid(token)} or expired ${isTokenExpired(token)}")
        return !isTokenExpired(token) && isTokenValid(token)
    }

    fun generateSessionToken(user: Users, sessionData: MutableMap<String, String>): String {
        val sessionId = UUID.randomUUID().toString()
        val sessionKey = "session:$sessionId"

        val session: MutableMap<String, String> = sessionData
        session["userId"] = user.id.toString()

        session.forEach { println("${it.key} ======><><><><><><><=== ${it.value}") }
        println("${user.getUsername()} ++++++++++++++++++ $sessionKey")

        redisTemplate.opsForHash<String, Any>().putAll(sessionKey, sessionData)
        redisTemplate.expire(sessionKey, jwtExpirationMs.toLong(), TimeUnit.MINUTES)

        val claims: MutableMap<String, Any> = HashMap()
        claims["role"] = user.role
        claims["sessionId"] = sessionId
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(user.getUsername())
            .setIssuedAt(Date())
            .setExpiration(Date(Date().time + jwtExpirationMs))
            .signWith(getSignInKey(), SignatureAlgorithm.HS256)
            .compact();
    }

    fun validatingToken(jwt: String, session: MutableMap<String, String>): Boolean {

        if (!isTokenValid(jwt))
            throw TokenRevokedException("TOKEN_LOGGED_OUT")

        val claims: Claims = Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(jwt).body

        val sessionId = claims["sessionId"] as String?
        val sessionKey = "session:$sessionId"

        val sessionData: Map<String, Any> = redisTemplate.opsForHash<String, Any>().entries(sessionKey)

        if (!sessionData.containsKey("userId")) {
            throw BadCredentialsException("Invalid session")
        }

        val userId: String = sessionData["userId"] as String
        val ipAddress = sessionData["ipAddress"] as String
        val userAgent = sessionData["User-Agent"] as String

        return ipAddress == session["ipAddress"] && userAgent == session["User-Agent"]

    }

    fun revokeToken(token: String) {
        val revokedTokenKey = "revokedTokens"
        redisTemplate.opsForSet().add(revokedTokenKey, token)
    }

    fun logout(jwt: String, refreshToken: String): Boolean {

        val claims: Claims = Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(jwt).body

        val sessionId = claims["sessionId"] as String?

        return if (sessionId != null) {
            redisTemplate.delete(sessionId)
            revokeToken(jwt)
            revokeToken(refreshToken)
            true
        } else false
    }

    fun isTokenValid(token: String): Boolean {
        val revokedTokenKey = "revokedTokens"
        return !redisTemplate.opsForSet().isMember(revokedTokenKey, token)!!
    }


}