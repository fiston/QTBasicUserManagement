package rw.qt.qtbasicusermanagement.exceptions


class InternalServerErrorException(message: String) : RuntimeException(message)

class ForbiddenException(message: String) : RuntimeException(message)

class NotFoundException(message: String) : RuntimeException(message)

class UnauthorizedException(message: String) : RuntimeException(message)

class InvalidJwtTokenException(message: String) : RuntimeException(message)

class BadRequestException(message: String) : RuntimeException(message)

class ParameterNeededException(message: String) : RuntimeException(message)

class DuplicateViolation(message: String):RuntimeException(message)

class AccountStatusException(message: String):RuntimeException(message)

class TokenRevokedException(message: String) : RuntimeException(message)