package rw.qt.qtbasicusermanagement.models.requests

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import rw.qt.qtbasicusermanagement.utils.PHONE_REGEX
import java.io.Serializable

data class UsersRequest (
    @get:NotBlank(message = "FIRSTNAME_CANNOT_BE_BLANK") var firstName: String,

    @get:NotBlank(message = "LASTNAME_CANNOT_BE_BLANK") var lastName: String,

    @get:NotBlank(message = "PHONE_NUMBER_CANNOT_BE_BLANK")
    @get:Pattern(regexp = PHONE_REGEX, message = "PHONE_NUMBER_NOT_VALID")
    var phoneNumber: String,

    @get:Email(message = "EMAIL_NOT_VALID") var email: String,

    @get:NotBlank(message = "USER_NAME_CANNOT_BE_BLANK") var username:String,

    @get:NotBlank(message = "PASSWORD_CANNOT_BE_BLANK") var password: String,

    ):Serializable

data class SelfPasswordResetRequest (
    val oldPassword: String,
    val newPassword: String
)

data class PasswordResetRequest (
    @get:NotBlank(message = "USERNAME_CANNOT_BE_BLANK") var username: String,
    @get:NotBlank(message = "PASSWORD_CANNOT_BE_BLANK") var newPassword: String
):Serializable