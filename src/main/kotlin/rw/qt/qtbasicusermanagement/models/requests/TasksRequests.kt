package rw.qt.qtbasicusermanagement.models.requests

import rw.qt.qtbasicusermanagement.utils.Priorities
import java.io.Serializable
import java.util.*

class TasksRequests (

    val title: String,
    val description: String,
    val status: String,
    val createdAt: Date,
    val updatedAt: Date,
    val assignees: String,
    val project: String,
    val priority: Priorities = Priorities.LOW,
    val fileAttachment: String
):Serializable