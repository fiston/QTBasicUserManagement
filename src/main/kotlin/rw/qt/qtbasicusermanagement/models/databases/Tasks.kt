package rw.qt.qtbasicTasksmanagement.models.databases



import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Table
import rw.qt.qtbasicusermanagement.utils.Priorities
import java.io.Serializable
import java.util.*


@Table("tasks") 
class Tasks(
    @Id
    @get:JvmName(name = "id_")
    var id: UUID? = null,
    var title: String,
    var description: String,
    var status: String,
    var createdAt: Date,
    var updatedAt: Date,

    var assignees:String? = null,
    var project: String,
    var priority: Priorities = Priorities.LOW,
    var fileAttachment: String
) : Serializable, Persistable<UUID> {

    @Transient
    var isNewTasks: Boolean = false
    
    override fun toString(): String {
        return "Tasks(" +
                "id=$id, " +
                "title='$title', " +
                "description='$description', " +
                "status='$status', " +
                "createdAt=$createdAt, " +
                "updatedAt=$updatedAt, " +
                "assignees=$assignees, " +
                "project='$project', " +
                "priority=$priority, " +
                "fileAttachment='$fileAttachment'" +
                ")"
    }

    override fun getId(): UUID? {
        return id ?: UUID.randomUUID()
    }

    override fun isNew(): Boolean {
        return id == null || isNewTasks
    }

    fun setAsNew(): Tasks {
        this.isNewTasks = true
        return this
    }
}