package rw.qt.qtbasicusermanagement.models.databases

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import org.springframework.data.annotation.*
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Table
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import rw.qt.qtbasicusermanagement.utils.ERole
import rw.qt.qtbasicusermanagement.utils.UserStatus

import java.util.*
import java.util.stream.Collectors

@Table
class Users(
    @Id
    @get:JvmName(name = "id_")
    var id: UUID? = UUID.randomUUID(),

    var firstName: String,

    @get:NotBlank(message = "LASTNAME_CANNOT_BE_BLANK") var lastName: String,


    @get:NotBlank(message = "PHONE_NUMBER_CANNOT_BE_BLANK") var phoneNumber: String,


    @get:Email(message = "EMAIL_NOT_VALID") var email: String,

    @get:JvmName("_getPassword")
    var password: String,

    @get:JvmName("_getUsername")
    var username: String,

    var role: ERole = ERole.ROLE_USER,

    ) : UserDetails, Persistable<UUID> {
    @Transient
    var isNewUser: Boolean = false

    @Version
    @Transient
    var version: Long = 0

    var status: UserStatus = UserStatus.PENDING_APPROVAL

    @CreatedDate
    var createdAt: Date = Date()

    @LastModifiedDate
    var updatedAt: Date = Date()

    var accountNonExpired: Boolean = true

    var accountNonLocked = true

    var credentialsNonExpired = true

    var enabled = false

    var isResettingPassword = false

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        val authorities: List<GrantedAuthority> = listOf(SimpleGrantedAuthority(role.name))
        return authorities.stream().map { authority -> SimpleGrantedAuthority(authority.authority) }
            .collect(Collectors.toList())
    }

    override fun getPassword(): String {
        return this.password
    }

    override fun getUsername(): String {
        return this.username
    }

    override fun isAccountNonExpired(): Boolean {
        return this.accountNonExpired
    }

    override fun isAccountNonLocked(): Boolean {
        return this.accountNonLocked
    }

    override fun isCredentialsNonExpired(): Boolean {
        return this.credentialsNonExpired
    }

    override fun isEnabled(): Boolean {
        return this.enabled
    }

    override fun getId(): UUID? {
        return id ?: UUID.randomUUID()
    }

    override fun isNew(): Boolean {
        return id == null || isNewUser
    }

    fun setAsNew(): Users {
        this.isNewUser = true
        return this
    }

    fun copy(
        firstName: String,
        lastName: String,
        phoneNumber: String,
        email: String,
        password: String,
        username: String,
        role: ERole,
        status: UserStatus,
        accountNonExpired: Boolean,
        accountNonLocked: Boolean,
        credentialsNonExpired: Boolean,
        enabled: Boolean
    ): Users {
        val users = Users(
            this.id,
            firstName,
            lastName,
            phoneNumber,
            email,
            password,
            username,
            role
        )
        users.version = version
        users.status = status
        users.createdAt = createdAt
        users.updatedAt = updatedAt
        users.accountNonExpired = accountNonExpired
        users.accountNonLocked = accountNonLocked
        users.credentialsNonExpired = credentialsNonExpired
        users.enabled = enabled
        return users
    }
}
