package rw.qt.qtbasicusermanagement.models.responses

import rw.qt.qtbasicusermanagement.utils.UserStatus


class RegistrationResponse (
     val message:String,
     val status: UserStatus,
     val id: String?
)