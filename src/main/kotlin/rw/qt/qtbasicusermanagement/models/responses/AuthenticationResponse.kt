package rw.qt.qtbasicusermanagement.models.responses

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.constraints.NotBlank
import java.util.*

class AuthenticationResponse(

    @NotBlank @JsonProperty("token") val token: String,
    @NotBlank @JsonProperty("refreshToken") val refreshToken: String,
    @NotBlank @JsonProperty("id") val id: String,
    @NotBlank @JsonProperty("username") val username: String,
    @NotBlank @JsonProperty("email") val email: String,
    @NotBlank @JsonProperty("phoneNumber") val phoneNumber: String,
    @NotBlank @JsonProperty("roles") val roles: String,
    @NotBlank @JsonProperty("roles") val isResettingPassword: Boolean
)