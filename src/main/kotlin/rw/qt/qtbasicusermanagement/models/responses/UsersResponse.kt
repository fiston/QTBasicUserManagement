package rw.qt.qtbasicusermanagement.models.responses



import rw.qt.qtbasicusermanagement.utils.ERole
import rw.qt.qtbasicusermanagement.utils.UserStatus
import java.io.Serializable
import java.util.*


data class UsersResponse(
    val id: UUID,
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val email: String,
    val password: String,
    val username: String,
    val role: ERole,
    val status: UserStatus,
    val accountNonExpired: Boolean,
    val accountNonLocked: Boolean,
    val credentialsNonExpired: Boolean,
    val enabled: Boolean
) : Serializable