package rw.qt.qtbasicusermanagement.models.responses

class HttpDefaultResponse(
    val status: String,
    val message: String,
    val timestamps: String
)