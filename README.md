# QT Basic User Management and Task Management

### Features

This application is a basic user management and task management application. It has the following features:

- User management that include the following:
    - User registration
    - User login
    - User logout
    - User profile update
    - User profile delete (soft delete)
    - User profile search
    - User profile list
    - Session management using Redis
- Task management that includes:
    - Task creation
    - Task list
    - Task search by id

### Reference Documentation

To run, use the following command:

```shell ./gradlew bootRun```

to compile, use the following command:

```shell ./gradlew build```

for environment variables, crate file .env in the root directory and add the following:

```shell 
R2DBC_URL=r2dbc:postgresql://127.0.0.1:5432/QT_Test
FLYWAY_DB_URL=jdbc:postgresql://127.0.0.1:5432/QT_Test?ssl=false
PROFILE=dev
SERVER_PORT=7889
R2DBC_USERNAME=postgres
R2DBC_PASSWORD=qGPZ079NZSxE9GC

REDIS_SERVER=127.0.0.1
REDIS_PASSWORD=DfH38Igz1EBjtXnI1P7KZhBKzIfNspuoSZOzAUH6HWv1NMKw0I
REDIS_PORT=6379
FILE_PATH=/tmp/QT_Test
```
PS: the above must be changed to mach your environment.

### Additional information

The application auto create the needed tables using flyway. 
Also, the application auto generate the needed data using V1__initialDB.sql file.

Default user:password are:
```admin:Admin@123```


### Future work and improvements

- log login and logout events
- add unit tests
